Planetary motion
================

In *Planetary motion*, you will create a multi-body gravitational simulation, which is entirely configurable through a user input file.

The project factorizes into
separate tasks quite naturally, but it is entirely up to you to decide
how to split up the work. The whole task can be done in standard Python alone, plus `matplotlib`_ for the visualization. Consider
using `numpy`_, too. The following sections will describe the problem in
some more detail, and will give some background information. 

Task description
----------------

You can break most of these constraints if you have a good reason. The
only rule that you *must* stick to is: **Have something to show on Friday!**


Setup
   Restrict the simulation to two dimensions:  *x* and *y* (plus time *t* of course).

Config file
   The user will provide a file, in this format::

      [Global]
      G = ...
      ...

      [Planet]
      name = ...
      mass = ...
      x_0 = ...
      y_0 = ...
      vx_0 = ...
      vy_0 = ...
      ...

      [Planet]
      ...

      [Planet]
      ...

      ...

   Do not hardcode any of the setup.

Physics
   The only interaction in the model is Newtonian gravity between all pairs of objects. 
   Do *not* hardcode any kinematic behaviour, all movement should come
   from the laws of mechanics and gravitation. 

   :doc:`More on numerical mechanics. <numerics>`

Performance / Accuracy
   The two main issues to take care of are performance scaling as the number of bodies goes up, and the accuracy of the simulation.
   Two-body gravitation has closed curve solutions. Your simulation should show that. 

Visualization 
   The system's movements should be shown as a matplotlib animation. 

   :doc:`More on matplotlib animation <animation>`.


.. _matplotlib: http://matplotlib.org/

.. _numpy: http://www.numpy.org/
