Day 2 --- Land or Water?
========================

Task 1: Map locations
---------------------

Write a program that takes geographic coordinates (such as 48.95˚N 9.13˚E), and prints out if that point is on land or on water. By convention, North and East are positive numbers.

* Familiarize yourself with the data set below. 
* Make a plan for how you want to approach this task. What sub-tasks need to be done?
* Estimate how much time each sub-task will take you before starting.
* Make frequent git commits while you work on the solution.
* You can work together on the concepts, but everyone should write their own individual copy of code 

Landcover data
~~~~~~~~~~~~~~

Helpful data for this task is this data set of global landcover with 1 km resolution (click to download):

* :download:`1km x 1km data set <gl-latlong-1km-landcover.bsq.bz2>`

* :download:`Info sheet describing the data layout <gl0500bs.txt>`

Download and then uncompress the data with e.g. ``bunzip2``. 


The data originates from http://www.landcover.org/data/landcover/. The University of Maryland Department of Geography generated this global land cover classification collection in 1998. Imagery from the AVHRR satellites acquired between 1981 and 1994 were analyzed to distinguish fourteen land cover classes at a resolution of 1 km [#f2]_ .

The encoding of the 14 different landcover types is shown here:

.. image:: landcover_legend.png

To load and handle the data set, Numpy is the perfect tool.
Find out more information about ``numpy.fromfile`` or ``numpy.memmap``, they will be useful in your task. What is the difference between them?

If you like, you can use ``matplotlib.imshow`` to visualise the loaded data. This can be a very useful cross-check. Do `not` plot the full data set, use a subsample by skipping points like this: ``data[::50,::50]``

.. image:: gl-latlong-1km-landcover.preview.jpg


.. [#f2] Hansen, M., R. DeFries, J.R.G. Townshend, and R. Sohlberg (1998), UMD Global Land Cover Classification, 1 Kilometer, 1.0, Department of Geography, University of Maryland, College Park, Maryland, 1981-1994.

  Hansen, M., R. DeFries, J.R.G. Townshend, and R. Sohlberg (2000), Global land cover classification at 1km resolution using a decision tree classifier, International Journal of Remote Sensing. 21: 1331-1365.


Task 2: Earthquake list
-----------------------

Decide for each earthquake in the following file if it occurred on land or on water and add that information as an additional semicolon-separated column to the data file:

* :download:`Earthquake data <events_4.5.txt>`

Task 3: Plot options
--------------------

Use ``argparse`` to add a command line option to your tool that plots the land cover type in a 10°x10° box around the requested position. This option does **not** need to work with a pipe.



Optional task: User input
-------------------------

Command line
~~~~~~~~~~~~

Modify your program to take user input on the command line::

	$ ./landcover 48.9 9.13
	Land

You can do this either by directly using ``sys.argv`` or, better, take a look at the ``argparse`` module.


You can make your python script executable like in the example here by a special first line 
of your script, that tells the Unix kernel which interpreter to use when the file is executed::

	#!/usr/bin/env python

Don't forget to set the ``x`` executable permission::

	$ chmod ugo+x ./landcover


Unix Pipe
~~~~~~~~~

Make your program useable as part of a UNIX pipe on the command line (you'll need ``sys.stdin``)::

	$ echo "54.95 -1.67" | ./landcover
	Land

	$ ./landcover
	0.0 0.0
	Water
	29.93 52.89
	Land
	[CTRL-D]
