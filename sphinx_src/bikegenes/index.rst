Genetic Bike
============

In *Genetic Bike*, you will create a simulation in Python to evolve an
optimal bicycle frame. 

.. The idea for this project comes from Peter
   Shadbolt's online implementation on his `QUBIT page`_. Have a look!

The project factorizes into
separate tasks quite naturally, but it is entirely up to you to decide
how to split up the work. The whole task can be done
in Python alone, using `matplotlib`_ for the visualization. Consider
using `numpy`_, too. The following sections will describe the problem in
some more detail, and will give some background information. 

Task description
----------------

You can break most of these constraints if you have a good reason. The
only rule that you *must* stick to is: **Have something to show on Friday!**



Geometry
   The simulation is restricted to two dimensions: distance *x* and height *h*
   (plus time *t* of course).

Bicycle
   The bike consists of four vertices. Two of them represent 
   wheels of an arbitrary size and small
   mass. The other two are more massive points that
   simulate the load of the rider on the frame. One of the
   wheels drives the frame.
   The four vertices are fully connected by six springs, with variable
   rest length and spring constants.
  
Ground
   A height profile of a road surface. To make the problem
   interesting, it must not be completely flat! Gravity exists and
   points to *--h*.
 
Physics
   Do *not* hardcode any kinematic behaviour, all movement should come
   from the laws of mechanics. You can assume that the driving wheel
   provides a constant acceleration and does not slip.

   :doc:`More on numerical mechanics. <numerics>`

Evolution
   The final shape of the bike *must not* be hardcoded! Starting from
   random initial configurations, the final shape should emerge
   through an evolutionary process of competitive selection. 

   :doc:`More on genetic algorithms <geneticalgos>`.

Visualization 
   Each bike's movement along the road in the evolution
   process should be shown as a matplotlib animation.

   :doc:`More on matplotlib animation <animation>`.


.. _matplotlib: http://matplotlib.org/

.. _numpy: http://www.numpy.org/

.. _QUBIT page: https://web.archive.org/web/20130515022037/http://www.qubit.devisland.net/ga/
