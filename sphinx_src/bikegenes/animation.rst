Animation example
=================

This small program shows how to do a simple animation in matplotlib:

.. literalinclude:: animation.py
 
