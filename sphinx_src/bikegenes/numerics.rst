Numerical mechanics
===================

For each point, you need to keep track of four numbers: the position and velocity in the two dimensions:

.. math:: x_i, y_i, v_{x,i}, v_{y,i}


In the simplest numerical solver, the positions are updated by adding the velocities:

.. math:: 
  x_i(t+\Delta t) = x_i(t) + v_{x,i} \Delta t 

The velocities are updated similarly by adding the accelerations in the different directions:

.. math:: 
  v_{x,i}(t+\Delta t) = v_{x,i}(t) + a_{x,i} \Delta t 

The actual physical behaviour of the system is encoded in the calculation of the 
accelerations :math:`a_{x,i}` at each time step. Sum up all forces acting in that direction, and divide by the mass of the point:

.. math:: 
  a_{x,i}(t) = \frac{ \sum_j{F_{x,ij}} }{ m_i }


Alternative methods
-------------------


Like all numeric methods, this `Euler method`_ has several pitfalls to
be aware of. The size of the numerical error at a given time scales 
with :math:`\Delta t`.

The Euler method is also not suited to oscillating problems (try it
out to see why!), you should use something like the `Runge-Kutta
fourth-order method`_ instead. At the cost of quadrupling the number
of calculations, the error of the numerical result scales much better:
with :math:`\Delta t^4`.



**Ask for help if needed, solving the physics is not the core task here!**



.. _Euler method: http://en.wikipedia.org/wiki/Euler_method
.. _Runge-Kutta fourth-order method: https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#The_Runge.E2.80.93Kutta_method


