Differential equation solvers
=============================

I created this page to experiment with :math:`\LaTeX` support in
Sphinx, but this collection of snippets should also be useful as
starting points to find out more about simulating the bicycle components.


Minimal ODE example: Radioactive decay
----------------------------------------

The change in the number of atoms :math:`N` in radioactive decay
follows the differential equation

.. math:: \dot{N}(t)=-\lambda N(t)\,.
   :label: decay

Even without knowing the analytical solution of this equation, we can
numerically approximate the behaviour of this system by rewriting
the mathematical definition of a derivative

.. math:: 
   \dot{N}(t) = \lim_{h\rightarrow 0}\frac{N(t+h)-N(t)}{h}
              \approx \frac{N(t+h)-N(t)}{h}

as

.. math::
   N(t+h) \approx N(t) + h \, \dot{N}(t)\,.

So far, this has been a mathematical identity. The physics information
comes in when we substitute :eq:`decay` for :math:`\dot{N}`:

.. math::
   N(t+h) \approx N(t) - h \lambda\, N(t)=(1-h\lambda)\,N(t)\,.

This form of the original problem allows us to step through the
solution in discrete time steps of :math:`h`, always updating the
state of the system :math:`N` from the system state at the previous
step.

Like all numeric methods, this `Euler method`_ has several pitfalls to
be aware of. Many of them already manifest themselves in this simple
example; let's look at one in particular: if you compare the numeric
solution to the analytic one, you will find that it
*always* undershoots (why?), and that the size of the error at a given
time scales with :math:`h`.

The Euler method is also not suited to oscillating problems (try it
out to see why!), you should use something like the `Runge-Kutta
fourth-order method`_ instead. At the cost of quadrupling the number
of calculations, the error of the numerical result scales much better:
with :math:`h^4`.

Second-order ODEs
-----------------

Most ODEs encountered in mechanics are of second order:
:math:`\ddot{\mathbf{x}}(t)=\mathbf{f}\left(\mathbf{x}(t),\dot{\mathbf{x}}(t),t\right)`, but the
solver methods decribed above only work for first-order problems. How
can we reconcile this discrepancy?

We can turn a single second-order ODE into two coupled first-order
ODEs by introducing the velocity as an independent variable.
The original equation then turns into

.. math:: \dot{\mathbf{v}}(t) = \mathbf{f}\left(\mathbf{x}(t),\mathbf{v}(t),t\right)

and

.. math:: \dot{\mathbf{x}}(t) = \mathbf{v}(t)\,,

both of which can be solved as above, with iterative steps for :math:`\mathbf{x}(t+h)` and :math:`\mathbf{v}(t+h)`.
   



Damped spring
-------------

The equation of motion for a damped harmonic oscillator with mass
:math:`m`, damping :math:`b` and spring constant :math:`D` is

.. math:: m\,\ddot{\mathbf{x}} + b\,\dot{\mathbf{x}} + D\,\mathbf{x} = 0\,.

You should do the rest.



.. _Euler method: http://en.wikipedia.org/wiki/Euler_method
.. _Runge-Kutta fourth-order method: https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#The_Runge.E2.80.93Kutta_method
