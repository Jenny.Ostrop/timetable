2022 NORBIS/DLN Collaborative programming school
================================================

In this repository, we'll include all relevant material, it will be updated frequently during the school.

Timetable
---------

This is the current day-by-day plan, but we may switch up some items at short notice.

Lunch is at 12:00 each day.

----

Day 1 - Monday
..............

* from 10:00 - registration
* 11:00 - Welcome and introduction. Short recap of Python
* 13:00 - Intro to git, getting-started Python exercises

Day 2 - Tuesday
...............

* 09:15 - Clean code & documentation
* 09:45 - Python libraries - numpy, scipy
* 13:00 - Numpy activity
* 15:00 - Git part 2: branching, merging, conflicts

 
`Landcover exercise <https://folk.uib.no/dgr061/cssd2022/landcover/>`_

Day 3 - Wednesday
.................

* 09:15 - High-level optimisation, data structures
* 10:45 - Code design, programming styles: OO / functional
* 13:00 - OO exercises

Day 4 - Thursday
................

* 09:15 - Language and hardware: assembly / C / Python
* 10:45 - mixing languages
* 13:00 - Makefiles for workflows
* 15:00 - language mixing exercises

Day 5 - Friday
..............

* 09:15 - Profiling
* 10:45 - Testing / debugging
* 13:00 - Git part 3 - conversational development
* 14:30 - Conv.devel./CI exercises

----

Day 6 - Monday
..............

* 09:15 - Project management
* 11:45 - Presentation of project tasks, choose project
* 13:00 - Form project groups, start project work

* `Genetic Bike <https://folk.uib.no/dgr061/cssd2022/bikegenes/>`_

   * https://git.app.uib.no/ii/collabschool22/genetic-bike-a
   * https://git.app.uib.no/ii/collabschool22/genetic-bike-b
   * https://git.app.uib.no/ii/collabschool22/genetic-bike-c

* `Mutation Analyser <https://folk.uib.no/dgr061/cssd2022/chasinscores/>`_

   * https://git.app.uib.no/ii/collabschool22/mutation-analyser-a
   * https://git.app.uib.no/ii/collabschool22/mutation-analyser-b

* `Traffic Jam <https://folk.uib.no/dgr061/cssd2022/trafficjam/>`_

   * https://git.app.uib.no/ii/collabschool22/traffic-jam

* `Planetary Motion <https://folk.uib.no/dgr061/cssd2022/planets/>`_

   * https://git.app.uib.no/ii/collabschool22/planetary-motion


Day 7 - Tuesday
...............

* 09:15 - Documentation tools
* 10:45 - Real world examples
* 13:00 - Real world examples: Norsk ordbok
* 13:30 - Project work

Day 8 - Wednesday
.................

* **09:10** - Medieval detour (**meeting point HF-bygget, Sydnesplassen 7**, see hf-bygget.png)
* ~11:15 - Packaging and distribution
* 13:00 - Project work

Day 9 - Thursday
................

* 09:15 - Copyright and licensing
* 10:45 - Citable code
* (upstairs room not available 12-15:00)
* 13:00 - Project work

Day 10 - Friday
...............

* 09:15 - Finalise presentations
* 09:45 - Present results
* 12:30 - End

----
